namespace Core.Domain.Models;

public class Entity
{
    public virtual int Id { get; protected set; }
    public long Version { get; protected set; }

    public override bool Equals(object obj)
    {
        if (obj == null || obj is not Entity)
        {
            return false;
        }

        if (ReferenceEquals(this, obj))
        {
            return true;
        }

        if (GetType() != obj.GetType())
        {
            return false;
        }

        var item = (Entity)obj;

        return item.Id == Id;
    }

    public static bool operator ==(Entity left, Entity right)
    {
        if (Equals(left, null))
        {
            return Equals(right, null);
        }
        else
        {
            return left.Equals(right);
        }
    }

    public static bool operator !=(Entity left, Entity right) => !(left == right);

    public override int GetHashCode() => Id.GetHashCode() ^ 31;
}
