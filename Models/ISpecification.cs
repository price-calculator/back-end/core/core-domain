
namespace Core.Domain.Models;

using System;
using System.Collections.Generic;
using System.Linq.Expressions;

public interface ISpecification<T> where T : Entity
{
    Expression<Func<T, bool>> Criteria { get; }
    List<Expression<Func<T, object>>> Includes { get; }
    List<string> IncludeStrings { get; }

    int Skip { get; }
    int Take { get; }
}
