namespace Core.Domain.Models;

using System;
using System.Collections.Generic;
using System.Linq.Expressions;

public abstract class BaseSpecification<T> : ISpecification<T>
    where T : Entity
{
    public Expression<Func<T, bool>> Criteria { get; }
    public List<Expression<Func<T, object>>> Includes { get; } = new List<Expression<Func<T, object>>>();
    public List<string> IncludeStrings { get; } = new List<string>();

    public int Skip { get; protected set; } = -1;
    public int Take { get; protected set; } = -1;

    protected BaseSpecification(Expression<Func<T, bool>> criteria) => Criteria = criteria;

    protected BaseSpecification()
    { }

    protected virtual void AddInclude(Expression<Func<T, object>> includeExpression) =>
        Includes.Add(includeExpression);

    protected virtual void AddInclude(string includeString) =>
        IncludeStrings.Add(includeString);
}
